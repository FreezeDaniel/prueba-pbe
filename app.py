# app.py
import os
from flask import Flask, request, render_template
import csv

app = Flask(__name__)

@app.route("/login", methods=['GET'])
def login():
    return render_template('login.html')

@app.route("/datosVehiculo", methods=['POST'])
def datos_vehiculo():
    username = request.form['username']
    password = request.form['password']
    return render_template('datosVehiculo.html', username=username, password=password)


@app.route("/guardarVehiculo", methods=['POST','GET'])
def guardarVehiculo():
    try:
        vehiculo = request.form['vehiculo']
        anio = request.form['anio']
        marca = request.form['marca']
        color = request.form['color']
        combustible = request.form['combustible']
        puertas = request.form['puertas']
        traccion = request.form['traccion']
        print([vehiculo, anio, marca, color, combustible, puertas, traccion])
        
        csv_filename = 'vehiculos.csv'
        # if not os.path.exists(csv_filename):
        # with open(csv_filename, mode='', newline='') as file:
        #     writer = csv.writer(file)
        #     writer.writerow(['vehiculo', 'anio', 'marca', 'color', 'combustible', 'puertas', 'traccion'])

        with open(csv_filename, mode='w', newline='') as file:
            writer = csv.writer(file)
            writer.writerow([vehiculo, anio, marca, color, combustible, puertas, traccion])
        
        return render_template('mostrarDatos.html', vehiculo=vehiculo, anio=anio, marca=marca,
                            color=color, combustible=combustible, puertas=puertas, traccion=traccion)
    except Exception as e:
        return str(e)

@app.route("/mostrarDatos", methods=['GET'])
def mostrar_datos():
    datos = []
    with open('vehiculos.csv', mode='r') as file:
        reader = csv.reader(file)
        for row in reader:
            datos.append(row)

    return render_template('mostrarDatos.html', datos=datos)

if __name__ == '__main__':
    app.run(debug=True)
